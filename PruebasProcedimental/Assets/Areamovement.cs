﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Areamovement : MonoBehaviour {

    [SerializeField]
    Rigidbody2D player;
    [SerializeField]
    int speed;
    [SerializeField]
    int jumpForce;
    [SerializeField]
    float positionX;
    public bool grounded = true;


    public bool Grounded
    {
        get
        {

            return grounded;
        }
        set
        {
            grounded = value;
        }

    }

    // Use this for initialization
    void Start()
    {

        positionX = Mathf.Abs(gameObject.transform.parent.position.x);

    }

    // Update is called once per frame
    void Update()
    {
        player.transform.position = new Vector3(player.transform.position.x + 0.2f, player.transform.position.y, player.transform.position.z);



    }

}
