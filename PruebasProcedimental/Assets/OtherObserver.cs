﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherObserver : MonoBehaviour {

    [SerializeField]
    player playerOne;
    [SerializeField]
    player playerTwo;
    [SerializeField]
    GameController c;

    GenericManager manager;



    // Use this for initialization
    void Start()
    {
        manager = GameObject.Find("Procedural Manager").GetComponent<GenericManager>();
        playerOne = GameObject.Find("PlayerOne").GetComponentInChildren<player>();
        playerTwo = GameObject.Find("PlayerTwo").GetComponentInChildren<player>();

    }

    // Update is called once per frame
    void Update()
    {
        if (!playerOne.hits.Equals(playerTwo.hits))
        {
            if ((playerOne.hits - playerTwo.hits)>2)
                manager.whoWins(30);
            else if ((playerTwo.hits - playerOne.hits)>2)
                manager.whoWins(-30);
            // c.change = false;
        }

    }
}
