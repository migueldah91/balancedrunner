﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour {

    public bool playerInside = false;
    public bool playerTwoInside = false;

    public bool PlayerInside
    {
        get
        {
            return playerInside;
        }
        set
        {
            playerInside = value;
        }
    }
    public bool PlayerTwoInside
    {
        get
        {
            return playerTwoInside;
        }
        set
        {
            playerTwoInside = value;
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
 //       Debug.Log(coll.gameObject.transform.parent.name);
        if (coll.CompareTag("Player") && coll.gameObject.transform.parent.name.Equals("PlayerOne")) playerInside = true;
        if (coll.CompareTag("Player") && coll.gameObject.transform.parent.name.Equals("PlayerTwo")) playerTwoInside = true;
        
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z);

	}
}
