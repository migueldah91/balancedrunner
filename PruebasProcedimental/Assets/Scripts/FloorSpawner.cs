﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSpawner : MonoBehaviour
{

    [SerializeField]
    GameObject floor;

    // Use this for initialization
    void Start()
    {
        spawn();
    }

    void spawn()
    {
       // Debug.Log(floor.transform.parent.name.Equals("PlayerOne"));
        //Debug.Log(transform.parent.transform.parent.name);
        if (transform.parent.transform.parent.name.Equals("PlayerOne"))
            Instantiate(floor, new Vector2(transform.position.x,-4.21f), Quaternion.identity);
        else if (transform.parent.transform.parent.name.Equals("PlayerTwo"))
            Instantiate(floor, new Vector2(transform.position.x, -13.5f), Quaternion.identity);
    
        Invoke("spawn", 1.6f);
    }
    // Update is called once per frame
    void Update()
    {

    }
}