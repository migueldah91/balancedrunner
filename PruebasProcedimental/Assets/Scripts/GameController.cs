﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : GameControllerInterface  {

    [SerializeField]
    GameObject spawnerPlayerOne;
    [SerializeField]
    GameObject spawnerPlayerTwo;
    [SerializeField]
    GenericManager genericManager;
    float t = 0.0f;

	// Use this for initialization
	void Start () {
		
	}

    

    public override void balancePlayerOne(string value)
    {
        if (value.Equals("player one"))
        {
            if (spawnerPlayerOne.GetComponent<Spawner>().MaxSpawnRate >= 1.5)
            {
                if (spawnerPlayerOne.GetComponent<Spawner>().MinSpawnRate >= 1)
                    spawnerPlayerOne.GetComponent<Spawner>().MinSpawnRate -= 0.5f;
                spawnerPlayerOne.GetComponent<Spawner>().MaxSpawnRate -= 0.5f;
                //state = PlayerStates.PlayerOneWins;
                Debug.Log("spawner cambiado");

            }

        }
    }

    public override void balancePlayerTwo(string value)
    {

        if (value.Equals("player two"))
        {
            if (spawnerPlayerTwo.GetComponent<Spawner>().MaxSpawnRate >= 1.5)
            {
                if (spawnerPlayerTwo.GetComponent<Spawner>().MinSpawnRate >= 1)
                    spawnerPlayerTwo.GetComponent<Spawner>().MinSpawnRate -= 0.5f;
                spawnerPlayerTwo.GetComponent<Spawner>().MaxSpawnRate -= 0.5f;
                //  state = PlayerStates.PlayerTwoWins;

            }
            
        }
 
           
    }

	// Update is called once per frame
	void Update () {
		
	}

}
