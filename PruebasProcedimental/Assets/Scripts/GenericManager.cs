﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericManager : MonoBehaviour {


    [SerializeField]
    GameControllerInterface controller;
    private float val,t, previous = 0;
    private bool low, medium, high, low2, medium2, high2 = false;

    public void cooldown(float time)
    {
        while (time > t)
            t += Time.deltaTime;
        time = 0.0f;
    }

    public float Val
    {
        get
        {
            return val;
        }
        set
        {
            val = value;
        }
    }
    
    // Use this for initialization
	void Start () {
		
	}
	
    public void whoWins(float percent)
    {
        //percent: -100 -> jugador uno gana por mucho
        //percent:    0 -> jugadores empatados
        //percent:  100 -> jugador dos gana por mucho
        previous = val;
        if (percent != 0)
            val += percent;
        else val = 0;


    }

	// Update is called once per frame
	void Update () {
		if(previous != val)
        {
            cooldown(2f);

            if(!val.Equals(0f))
            {
                if (val >= -31 && val < 0 && !low)
                {
                    low = true;
                    controller.balancePlayerOne("player one");

                }
                if (val > 0 && val < 31 && !low2)
                    Debug.Log("gana 2");
                    low2 = true;
                    controller.balancePlayerTwo("player two");
                }
                if (val >= -61 && val < 0 && !medium)
                {

                    medium = true;
                    controller.balancePlayerOne("player one");
                }
                if (val > 0 && val < 61 && !medium2)
                {
                    Debug.Log("gana 2");
                    medium2 = true;
                    controller.balancePlayerTwo("player two");
                }
            }
    
        }/*
        if(val >=-100 && val <=-50)
        {
            cooldown(1);
            controller.balancePlayerOne("player two");
        }
        else if(val >=-50 && val <= 0)
        {
            cooldown(2);
            controller.balancePlayerOne("player two");
        }
        else if(val >= 0 && val <= 50)
        {
            cooldown(2);
            controller.balancePlayerTwo("player one");
        }
        else
        {
            cooldown(1);
            controller.balancePlayerTwo("player one");
        }*/

}
