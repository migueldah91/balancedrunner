﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {

    [SerializeField]
    GameObject player;
    private RaycastHit2D ray;
    [SerializeField]
    Transform t;
	// Use this for initialization
	void Start () 
    {
	}
	/*

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Floor"))
        {
            player.GetComponent<PlayerMovement>().Grounded = true;

        }
            
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.CompareTag("Floor"))
            player.GetComponent<PlayerMovement>().Grounded = false;
    }
    */

	// Update is called once per frame
	void Update () {

        ray = Physics2D.Raycast(t.position, Vector2.down, Mathf.Infinity);
        //Debug.Log(ray.distance);
        if (ray.distance.Equals(0) ? player.GetComponent<PlayerMovement>().Grounded = true : player.GetComponent<PlayerMovement>().Grounded = false) ;

	}
}
