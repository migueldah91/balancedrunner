﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour {

    
    [SerializeField]
    GameObject firstArea;
    [SerializeField]
    GameObject secondArea;
    Camera c;
    [SerializeField]
    Camera c2;

    DetectPlayer insidePj1;
    DetectPlayer insidePj2;
    DetectPlayer secondAreaPj1;
    DetectPlayer secondAreaPj2;

    [SerializeField]
    GenericManager manager;
   
    private bool firstTriggerOne = false;
    private bool firstTriggerTwo = false;
    private bool secondAreaOne = false;
    private bool secondAreaTwo = false;

    public bool InsidePj1
    {
        get
        {
            return insidePj1.PlayerInside;
        }

    }

    public bool InsidePj2
    {
        get
        {
            return insidePj2.PlayerTwoInside;
        }

    }
    public bool SecondAreaPj1
    {
        get
        {
            return secondAreaPj1.PlayerInside;
        }

    }

    public bool SecondAreaPj2
    {
        get
        {
            return secondAreaPj2.PlayerTwoInside;
        }

    }

    // Use this for initialization
	void Start () {
        insidePj1 = firstArea.GetComponent<DetectPlayer>();
        insidePj2 = firstArea.GetComponent<DetectPlayer>();
        secondAreaPj1 = secondArea.GetComponent<DetectPlayer>();
        secondAreaPj2 = secondArea.GetComponent<DetectPlayer>();
	}
	
	// Update is called once per frame
	void Update () {

        if(InsidePj1 && !InsidePj2 && !firstTriggerOne)
        {
            manager.whoWins(30);
            firstTriggerOne = true;
         //   Debug.Log("Gana 1");
        }
        else if (!InsidePj1 && InsidePj2 && !firstTriggerOne)
        {
            manager.whoWins(-30);
            firstTriggerOne = true;
         //   Debug.Log("Gana 2");
        }
        else if (InsidePj1 && InsidePj2)
        {
            manager.whoWins(0);
            firstTriggerOne = false;
          //  Debug.Log("Gana 0");
        }
        else if (SecondAreaPj1 && !SecondAreaPj2 && !firstTriggerTwo)
        {
            manager.whoWins(60);
            firstTriggerTwo = true;
            //   Debug.Log("Gana 1");
        }
        else if (!SecondAreaPj1 && SecondAreaPj2 && !firstTriggerTwo)
        {
            manager.whoWins(-60);
            firstTriggerTwo = true;
            //   Debug.Log("Gana 2");
        }
        else if (SecondAreaPj1 && SecondAreaPj2)
        {
            manager.whoWins(0);
            firstTriggerTwo = false;
            //  Debug.Log("Gana 0");
        }

    
	}
}
