﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerStates {

    Start = 0, PlayerOneWins = 1, PlayerTwoWins = 2, Deuce = 3
}
