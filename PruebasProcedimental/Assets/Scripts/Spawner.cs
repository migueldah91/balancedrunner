﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField]
    GameObject[] spawn;
    [SerializeField]
    GameObject[] resources;
    [SerializeField]
    float minSpawnRate = 0.0f;
    [SerializeField]
    float maxSpawnRate;
    float timer = 0.0f;
    public float Timer
    {
        get
        {
            return timer;
        }
        set
        {
            timer = value;
        }
    }
    public float MinSpawnRate
    {
        get
        {
            return minSpawnRate;
        }
        set
        {
            minSpawnRate = value;
        }
    }
    public float MaxSpawnRate
    {
        get
        {
            return maxSpawnRate;
        }
        set
        {
            maxSpawnRate = value;
        }
    }


	// Use this for initialization
	void Start () {
        spawnProps();
	}

    private void spawnProps()
    {
        Instantiate(resources[UnityEngine.Random.Range(0, resources.Length)], spawn[UnityEngine.Random.Range(0, spawn.Length)].transform.position, Quaternion.identity);
        Invoke("spawnProps", UnityEngine.Random.Range(minSpawnRate, maxSpawnRate));
    }
	// Update is called once per frame
	void Update () {
        // transform.position = new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z);

	}
}
